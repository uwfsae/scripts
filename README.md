### What is this repository for? ###

* Scripts for CAD Lab PCs

### How do I get set up? ###
* Copy runscripts.ps1 to C:\Scripts
* run in powershell 
```
Set-ExecutionPolicy RemoteSigned
Unblock-File C:\Scripts\runscripts.ps1
```
* Windows task scheduler: Create task
* Name: Logout inactive users
* Change user or group: SYSTEM
* Check: Run with highest privileges
* Triggers tab: New: One time: Set to some time in the future (like 10 min): repeat task every 1 hour: for indefinitely: stop task if runs longer than 30 mins
* Actions tab: New: Start a program: 
```
Powershell .\runscripts.ps1
```
start in 
```
C:\Scripts
```
* Conditions: Uncheck most
* OK

### Who do I talk to? ###

* IT Management
* Michael Lee
* Jeff Pyke