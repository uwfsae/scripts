<#
.SYNOPSIS
    Clears the printer spool.
.DESCRIPTION
    Clears the printer spool, in case some documents have become
    stuck + cannot be printed. Requires admin access to run.
    
You can run this script directly on Powershell:

        C:\> .\Clear-Printer-Spool.ps1

    Note: If it complains that "execution of scripts is disabled 
    on this system", then first open up Powershell as an administrator 
    and run the following command:

        C:\> Set-ExecutionPolicy RemoteSigned

    ...and try again.

    Email mlee42@uw.edu for any further questions you might have.
.PARAMETER help
    Displays this help
#>
param (
  [switch]$help = $false
)

# Start of main code
if ($help) {
    Get-Help $MyInvocation.MyCommand.Path
} else {
    net stop spooler
    Write-Output "Clearing spool..."
    Write-Output ""
    del $env:SystemRoot\system32\spool\printers\*.shd
    del $env:SystemRoot\system32\spool\printers\*.spl
    net start spooler
}
